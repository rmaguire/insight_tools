#!/usr/bin/env python
import numpy as np
from numpy.distutils.core import setup
from setuptools import find_packages

setup(name='insight_tools',
      version='0.1',
      description='various tools for processing insight data',
      author='Ross Maguire',
      author_email='rmaguir@umd.edu',
      packages = find_packages(),
      scripts=['scripts/insight_ascii_to_sac'],
      install_requires = ['obspy'],
      url='www.gitlab.com/rmaguire/insight_tools')
